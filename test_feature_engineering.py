import phoenixdb
import phoenixdb.cursor
import avro.schema
import io

from avro.datafile import DataFileWriter
from avro.io import DatumReader, BinaryDecoder, DatumWriter, BinaryEncoder
from io import BytesIO
from kafka import KafkaConsumer, KafkaProducer


database_url = 'http://192.168.2.238:8765/'
conn = phoenixdb.connect(database_url, autocommit=True)

cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS arlita")
cursor.execute("""CREATE TABLE arlita (
SOURCE_FILE VARCHAR,
      NAMA_FITUR VARCHAR,
      NAMA_KATEGORI VARCHAR,
      NAMA_GRUP VARCHAR,
      NAMA_SUB_GRUP VARCHAR,
      KODE_REGION VARCHAR,
      NAMA_REGION VARCHAR,
      DESKRIPSI_REGION VARCHAR,
      KODE_CABANG VARCHAR,
      NAMA_CABANG VARCHAR,
      NAMA_UNIT VARCHAR,
      KETERANGAN_TRX VARCHAR,
      KETERANGAN_LOKASI VARCHAR,
      LOKASI_TRX VARCHAR,
      NAMA_PENGIRIM VARCHAR,
      BANK_PENGIRIM VARCHAR,
      REKENING_SUMBER VARCHAR,
      TIPE_REKENING_SUMBER VARCHAR,
      NAMA_PENERIMA VARCHAR,
      REKENING_PENERIMA VARCHAR,
      KODE_BANK_PENERIMA VARCHAR,
      KODE_MATA_UANG VARCHAR,
      SISA_UANG DOUBLE,
      JUMLAH_TRX DOUBLE,
      FEE_TRANSAKSI INTEGER,
      KODE_PROC INTEGER,
      KODE_RESPON VARCHAR,
      KODE_RESPON_DESC VARCHAR,
      TANGGAL_TRX VARCHAR,
      WAKTU_TRX UNSIGNED_LONG NOT NULL,
      ID_TERMINAL VARCHAR,
      NO_KARTU VARCHAR,
      TAHUN VARCHAR,
      BULAN VARCHAR,
      HARI VARCHAR,
      JAM VARCHAR,
      MENIT VARCHAR,
      DETIK VARCHAR,
      ID_FITUR INTEGER,
      TIPE_TERMINAL VARCHAR,
      DIFF_TXAMOUNT_BEFORE DOUBLE,
      TXAMOUNT_DAYSUM DOUBLE,
      CEKSALDO_BEFORE BOOLEAN,
      DIFF_TIME UNSIGNED_LONG,
      SEQ_TRXDAY VARCHAR,
      RESPONSECODE_BEFORE VARCHAR,
      CONSTRAINT PK PRIMARY KEY(REKENING_SUMBER, TANGGAL_TRX, WAKTU_TRX))""")

#cursor.execute("UPSERT INTO arlita VALUES (REKENING_SUMBER, TANGGAL_TRX, WAKTU_TRX, )", ('121000006', '01/29/2019', '1800', ''))
#cursor.execute("SELECT * FROM arlita")
#print(cursor.fetchall())

consumer_schema = avro.schema.Parse(open("avro-schema-fix.avsc").read())
reader = avro.io.DatumReader(consumer_schema)

#Decoder module: A module for decoding bytearray into dictionary based on schema
def decode(msg_value):
	message_bytes = io.BytesIO(msg_value)
	decoder = BinaryDecoder(message_bytes)
	even_dict = reader.read(decoder)
	return even_dict

#	STREAM DATA FROM SUBSCRIBED TOPIC 'test' AS KAFKA CONSUMER
consumer = KafkaConsumer('fds_unscored', bootstrap_servers='192.168.2.248:9092')

def CekSaldoBefore(rekening_sumber, tanggal_trx, tipe_terminal, nama_grup):
    queryCek = "SELECT * FROM arlita WHERE REKENING_SUMBER=? AND TANGGAL_TRX=? AND TIPE_TERMINAL=? AND NAMA_GRUP='Cek Saldo' ORDER BY WAKTU_TRX DESC"
    data = rekening_sumber, tanggal_trx, tipe_terminal, nama_grup
    cur = conn.cursor()
    cur.execute(queryCek, data)
    result = cur.fetchone()
    if(result != None):
        ceksaldo_before = True
    else:
        ceksaldo_before = False
    return ceksaldo_before

def ubahJenisKartu(rekening_sumber):
    last_character = rekening_sumber[-1:]
    convert_char = int(last_character)
    if (convert_char % 2 == 0):
        jenis_kartu = "SIMPEDES"
    else:
        jenis_kartu = "PRIORITAS"
    return jenis_kartu

def splitNamaGrup(nama_grup):
    if nama_grup == "CekSaldo":
        nama_grup = "Cek Saldo"
    elif nama_grup == "TarikTunai":
        nama_grup = "Tarik Tunai"
    elif nama_grup == "TransferBRI":
        nama_grup = "Transfer BRI"
    elif nama_grup == "TransferBRIVA":
        nama_grup = "Transfer BRIVA"
    else:
        nama_grup = "Transfer Bersama"
    return nama_grup

def lokasiTRX(kode_mata_uang):
    if kode_mata_uang == '360':
        lokasi_trx = "Dalam Negeri"
    else:
        lokasi_trx = "Luar Negeri"
    return lokasi_trx

#	PARSE KAFKA CONSUMER INTO DICTIONARY
for msg in consumer:
    msg_value = msg.value
    even_dict = decode(msg_value)

    rekening_sumber = even_dict["REKENING_SUMBER"]
    source_file = even_dict["SOURCE_FILE"]
    nama_fitur = even_dict["NAMA_FITUR"]
    nama_kategori = even_dict["NAMA_KATEGORI"]
    nama_sub_grup = even_dict["NAMA_SUB_GRUP"]
    kode_region = even_dict["KODE_REGION"]
    nama_region = even_dict["NAMA_REGION"]
    deskripsi_region = even_dict["DESKRIPSI_REGION"]
    kode_cabang = even_dict["KODE_CABANG"]
    nama_cabang = even_dict["NAMA_CABANG"]
    nama_unit = even_dict["NAMA_UNIT"]
    keterangan_trx = even_dict["KETERANGAN_TRX"]
    keterangan_lokasi = even_dict["KETERANGAN_LOKASI"]
    nama_pengirim = even_dict["NAMA_PENGIRIM"]
    bank_pengirim = even_dict["BANK_PENGIRIM"]
    rekening_sumber = even_dict["REKENING_SUMBER"]
    nama_penerima = even_dict["NAMA_PENERIMA"]
    rekening_penerima = even_dict["REKENING_PENERIMA"]
    kode_bank_penerima = even_dict["KODE_BANK_PENERIMA"]
    sisa_uang = even_dict["SISA_UANG"]
    jumlah_trx = even_dict["JUMLAH_TRX"]
    fee_transaksi = even_dict["FEE_TRANSAKSI"]
    kode_proc = even_dict["KODE_PROC"]
    kode_respon = even_dict["KODE_RESPON"]
    kode_respon_desc = even_dict["KODE_RESPON_DESC"]
    tanggal_trx = even_dict["TANGGAL_TRX"]
    waktu_trx = even_dict["WAKTU_TRX"]
    id_terminal = even_dict["ID_TERMINAL"]
    no_kartu = even_dict["NO_KARTU"]
    tahun = even_dict["TAHUN"]
    bulan = even_dict["BULAN"]
    hari = even_dict["HARI"]
    jam = even_dict["JAM"]
    menit = even_dict["MENIT"]
    detik = even_dict["DETIK"]
    id_fitur = even_dict["ID_FITUR"]
    tipe_terminal = even_dict["TIPE_TERMINAL"]
    #diff_txamount_before = even_dict["DIFF_TXAMOUNT_BEFORE"]
    #txamount_daysum = even_dict["TXAMOUNT_DAYSUM"]
    #ceksaldo_before = even_dict["CEKSALDO_BEFORE"]
    #diff_time = even_dict["DIFF_TIME"]
    seq_trxday = "1"
    responsecode_before = "-"

    even_dict["TIPE_REKENING_SUMBER"] = ubahJenisKartu(rekening_sumber)
    tipe_rekening_sumber = even_dict["TIPE_REKENING_SUMBER"]

#       print(even_dict)
    nama_grup = even_dict["NAMA_GRUP"]
    even_dict["NAMA_GRUP"] = splitNamaGrup(nama_grup)
    nama_grup = even_dict["NAMA_GRUP"]

    even_dict["CEKSALDO_BEFORE"] = CekSaldoBefore(rekening_sumber, tanggal_trx, tipe_terminal, nama_grup)
    ceksaldo_before = even_dict["CEKSALDO_BEFORE"]

    kode_mata_uang = even_dict["KODE_MATA_UANG"]
    even_dict["LOKASI_TRX"] = lokasiTRX(kode_mata_uang)
    lokasi_trx = even_dict["LOKASI_TRX"]

    query = ("""UPSERT INTO arlita (
    SOURCE_FILE,
    NAMA_FITUR,
    NAMA_KATEGORI,
    NAMA_GRUP,
    NAMA_SUB_GRUP,
    KODE_REGION,
    NAMA_REGION,
    DESKRIPSI_REGION,
    KODE_CABANG,
    NAMA_CABANG,
    NAMA_UNIT,
    KETERANGAN_TRX,
    KETERANGAN_LOKASI,
    NAMA_PENGIRIM,
    BANK_PENGIRIM,
    REKENING_SUMBER,
    TIPE_REKENING_SUMBER,
    NAMA_PENERIMA,
    REKENING_PENERIMA,
    KODE_BANK_PENERIMA,
    KODE_MATA_UANG,
    SISA_UANG,
    JUMLAH_TRX,
    FEE_TRANSAKSI,
    KODE_PROC,
    KODE_RESPON,
    KODE_RESPON_DESC,
    TANGGAL_TRX,
    WAKTU_TRX,
    ID_TERMINAL,
    NO_KARTU,
    TAHUN,
    BULAN,
    HARI,
    JAM,
    MENIT,
    DETIK,
    ID_FITUR,
    TIPE_TERMINAL,
    DIFF_TXAMOUNT_BEFORE,
    TXAMOUNT_DAYSUM,
    CEKSALDO_BEFORE,
    DIFF_TIME,
    SEQ_TRXDAY,
    RESPONSECODE_BEFORE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""")

    data = source_file, nama_fitur, nama_kategori, nama_grup, nama_sub_grup, kode_region, nama_region, deskripsi_region, kode_cabang, nama_cabang, nama_unit, keterangan_trx, keterangan_lokasi, nama_pengirim, bank_pengirim, rekening_sumber, tipe_rekening_sumber, nama_penerima, rekening_penerima, kode_bank_penerima, kode_mata_uang, sisa_uang, jumlah_trx, fee_transaksi, kode_proc, kode_respon, kode_respon_desc, tanggal_trx, waktu_trx, id_terminal, no_kartu, tahun, bulan, hari, jam, menit, detik, id_fitur, tipe_terminal, 0, 0, ceksaldo_before, 0, seq_trxday, responsecode_before

    cur = conn.cursor()
    cur.execute(query, data)
    conn.commit()

    print(data)

#cursor.execute("SELECT * FROM arlita")
#print(cursor.fetchall())
# cursor = conn.cursor(cursor_factory=phoenixdb.cursor.DictCursor)
# cursor.execute("SELECT * FROM arlita WHERE REKENING_PENERIMA='121000006'")
